﻿using System;
using System.Collections.Generic;

namespace testappaspnetcore.DAL
{
    public partial class Client
    {
        public long Id { get; set; }
        public string PersonType { get; set; }
        public string CorporateName { get; set; }
        public string PhantasyName { get; set; }
        public string Names { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }
        public string MarriedSurname { get; set; }
        public string IdentityDocument { get; set; }
        public string WebSite { get; set; }
        public string Address { get; set; }
        public string AddressNumber { get; set; }
        public string AddressInteriorNumber { get; set; }
        public string Block { get; set; }
        public string Lot { get; set; }
        public string UrbanizationDescription { get; set; }
        public string Phone { get; set; }
        public decimal Code { get; set; }
        public string Email { get; set; }
        public string Status { get; set; }
        public DateTime StatusDate { get; set; }
        public decimal IdentityDocumentTypeId { get; set; }
        public decimal? EconomicActivityId { get; set; }
        public decimal? DepartmentId { get; set; }
        public decimal StateId { get; set; }
        public decimal DistrictId { get; set; }
        public decimal? StreetTypeId { get; set; }
        public decimal? UrbanizationTypeId { get; set; }
        public decimal StatusUserId { get; set; }
        public decimal? VoidReferenceId { get; set; }
        public decimal? SunatCiiuId { get; set; }
        public bool? IsBillingAutomatic { get; set; }
    }
}
