﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using testappaspnetcore.Models;
using testappaspnetcore.DAL;

namespace testappaspnetcore.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Client()
        {
            _logger.LogInformation("Consulta iniciada.");
            List<ClientViewModel> model = new List<ClientViewModel>();
            using (ModelContext context = new ModelContext())
            {
                model = context.Client.Take(5).Select( x => new ClientViewModel()
                {
                    Id = x.Id,
                    CorporateName = x.CorporateName
                }).ToList();
            }
            _logger.LogInformation("Consulta finalizada.");
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
